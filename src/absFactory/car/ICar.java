package absFactory.car;

public interface ICar {
    public String getOwner(String name);
}
