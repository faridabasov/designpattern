package absFactory.color;

public interface Color {
    public String getColor(String name);
}
