package absFactory;

import absFactory.car.BMW;
import absFactory.car.ICar;
import absFactory.car.Toyota;
import absFactory.color.BMWColor;
import absFactory.color.Color;
import absFactory.color.ToyotaColor;

public class FactoryCreator {
    public static ICar getFactoryOfCar(String factoryType){
        if(factoryType.equals("BMW")){
            return new BMW();
        }else if(factoryType.equals("Toyota")){
            return new Toyota();
        } else return null;
    }

    public static Color getFactoryOfColor(String factoryType){
        if(factoryType.equals("BMW")){
            return new BMWColor();
        }else if(factoryType.equals("Toyota")){
            return new ToyotaColor();
        } else return null;
    }
}
