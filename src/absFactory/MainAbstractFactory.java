package absFactory;



public class MainAbstractFactory {
    static FactoryCreator factoryCreator;

    public static void main(String[] args){
        System.out.println(factoryCreator.getFactoryOfCar("Toyota").getOwner(""));
        System.out.println(factoryCreator.getFactoryOfColor("Toyota").getColor(""));
        System.out.println(factoryCreator.getFactoryOfCar("BMW").getOwner(""));
        System.out.println(factoryCreator.getFactoryOfColor("BMW").getColor(""));

    }
}
