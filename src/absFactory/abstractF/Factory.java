package absFactory.abstractF;

import absFactory.car.BMW;
import absFactory.car.ICar;
import absFactory.car.Toyota;
import absFactory.color.BMWColor;
import absFactory.color.Color;
import absFactory.color.ToyotaColor;

public class Factory implements AbstractFactory {
    @Override
    public ICar getCar(String car) {
        if (car.equals("BMW")) {
            return new BMW();
        } else if (car.equals("Toyota")) {
            return new Toyota();
        }
        return null;
    }

    @Override
    public Color getColor(String car) {
        if (car.equals("BMW")) {
            return new BMWColor();
        } else if (car.equals("Toyota")) {
            return new ToyotaColor();
        }
        return null;
    }
}
