package absFactory.abstractF;

import absFactory.car.ICar;
import absFactory.color.Color;

public interface AbstractFactory {
    public ICar getCar(String car);

    public Color getColor(String car);
}
