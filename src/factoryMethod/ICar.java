package factoryMethod;

public interface ICar {
    public String getCarNumber(int id);
}
