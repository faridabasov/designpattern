package singleton;

public class Singleton {

    private static Singleton singleton;

    private Singleton(){
    }

    public static Singleton getInstagce(){
        if(singleton == null){
            synchronized (Singleton.class){
                if (singleton == null){
                    singleton = new Singleton();
                }
            }
        }
        return singleton;
    }
}
