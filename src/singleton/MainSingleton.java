package singleton;

public class MainSingleton {

    public static void main(String[] args) {
        Singleton mainSingleton1 = Singleton.getInstagce();
        Singleton mainSingleton2 = Singleton.getInstagce();
        Singleton mainSingleton3 = Singleton.getInstagce();

        System.out.println(mainSingleton1);
        System.out.println(mainSingleton2);
        System.out.println(mainSingleton3);
    }

}
