package builder;

public class MainBuilder {
    public static void main(String[] args){
        Book book = Book.builder().name("JAVA").writer("Farid").language("az");

        System.out.println(book.getName());
        System.out.println(book.getWriter());
        System.out.println(book.getLanguage());
    }
}
