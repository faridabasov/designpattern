package builder;

public class Book {
    private String name;
    private String writer;
    private String language;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Book build(){
        return this;
    }

    public static Book builder(){
        return new Book();
    }

    public Book name(String name){
        this.setName(name);
        return this;
    }
    public Book writer(String writer){
        this.setWriter(writer);
        return this;
    }
    public Book language(String language){
        this.setLanguage(language);
        return this;
    };
}
